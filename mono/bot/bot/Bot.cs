﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

namespace bot
{
    public class Bot
    {
        private static double MAX_SPEED 
        {
            get { return MAX_SPEED_CALCULADA == 0 ? MAX_SPEED_DEFAULT : MAX_SPEED_CALCULADA; }
            set { MAX_SPEED_CALCULADA = value; }
        }
        public static double MAX_SPEED_X { get { return MAX_SPEED * 10; } }
        private static double MAX_SPEED_DEFAULT = 20;
        public static double MAX_SPEED_DEFAULT_CURVA = 6;
        public static double MAX_SPEED_CALCULADA;

        private bot.init.Track track { get { return gameInit.data.race.track; } }
        private init.GameInit gameInit;
        private StreamWriter writer;
        private PiecePosition posAnterior;
        private PiecePosition posActual;
        double perfectThrottle = 1;
        private double velocidad;
        private double aceleracion;
        private double desaceleracion;
        private double friccion;
        private double calcFriccionSpeed = 0;
        private double velocidadAnterior;
        private bool calcularFriccion = false;
        private bool anguloAumentando = false;
        private Datos me;
        private double ultimoAngulo = 0;
        int[] bestLane;
        private Mensajes.In.TurboAvailable turboAvailable;
        private int rectaMasLargaIndex = 0;
        private bool switchear;
        List<Curva> curvas;
        Curva curvaActual = null;
        double[,] piezaLineaVelocidad { get { return SpeedManager.piezaLineaVelocidad; } }

        public Bot(StreamReader reader, StreamWriter writer, SendMsg join)
        {
            this.writer = writer;
            string line;

            send(join);

            string lastType = string.Empty;

            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

                if (msg.msgType != lastType)
                {
                    lastType = msg.msgType;
                }

                switch (msg.msgType)
                {
                    case "carPositions":
                        //->
                        //Lógica de carrera
                        CarPositions carPositions = JsonConvert.DeserializeObject<CarPositions>(line);
                        me = carPositions.data.First();

                        anguloAumentando = Math.Abs(ultimoAngulo) < Math.Abs(me.angle);
                        ultimoAngulo = me.angle;

                        updateSpeed(me.piecePosition);
                        updateAceleracion();

                        perfectThrottle = getNextThrottle();
                        if (velocidad > calcFriccionSpeed && !calcularFriccion)
                        {
                            perfectThrottle = 0;
                            calcularFriccion = true;
                        }
                        calcularFricciom();

                        SendMsg comando = new Throttle(perfectThrottle, carPositions.gameTick);

                        switchear = posAnterior != null && posAnterior.pieceIndex != posActual.pieceIndex && track.pieces[getNextPieceIndex(posActual.pieceIndex)].@switch == true;

                        if (turboAvailable != null && posActual.pieceIndex == rectaMasLargaIndex)
                        {
                            comando = new Turbo();
                            Console.WriteLine("Turbo lanzado!");
                            turboAvailable = null;
                        }
                        else if (switchear)
                        {
                            if (bestLane[getNextPieceIndex( posAnterior.pieceIndex ) + posAnterior.lap * track.pieces.Count] < bestLane[getNextPieceIndex(posActual.pieceIndex) + posActual.lap * track.pieces.Count])
                            {
                                comando = new SwitchLane("Right", carPositions.gameTick);
                                Console.WriteLine("Right!");
                            }
                            else if (bestLane[getNextPieceIndex(posAnterior.pieceIndex) + posAnterior.lap * track.pieces.Count] > bestLane[getNextPieceIndex(posActual.pieceIndex) + posActual.lap * track.pieces.Count])
                            {
                                comando = new SwitchLane("Left", carPositions.gameTick);
                                Console.WriteLine("Left!");
                            }
                            switchear = false;
                        }

                        
                        Console.Write(carPositions.gameTick.ToString("0000") + ": " + me.piecePosition.lap + " | " + me.piecePosition.pieceIndex + " | " + me.piecePosition.inPieceDistance.ToString("#00"));
                        if(me.angle != 0)
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.Write(" ( " + me.angle.ToString("#0") + " )");
                            Console.ForegroundColor = ConsoleColor.Gray;

                        }

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(" [ " + perfectThrottle.ToString("#0.00") + " ]");
                        Console.ForegroundColor = ConsoleColor.Gray;

                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.Write(" v" + velocidad.ToString("#0.00") );
                        Console.ForegroundColor = ConsoleColor.Gray;

                        if (nextCurva(me.piecePosition.pieceIndex))
                        {
                            Console.Write(" NC");
                        }
                        if (!enRecta(me.piecePosition.pieceIndex))
                        {
                            //TODO Lenght?
                            setPieceLenght(me.piecePosition.pieceIndex, me.piecePosition.lane.startLaneIndex);

                            Console.Write(" C:" + track.pieces[me.piecePosition.pieceIndex].angle + " | " + track.pieces[me.piecePosition.pieceIndex].length.ToString("#0"));
                        }

                        //mejorar velocidad para curvas
                        if (curvaActual == null)
                        {
                            curvaActual = curvas.Where(x => x.enEsta(me.piecePosition.pieceIndex)).FirstOrDefault();
                        }
                        else if (curvaActual != null && curvas.Where(x=>x.enEsta(me.piecePosition.pieceIndex)).FirstOrDefault() != curvaActual )
                        {
                            SpeedManager.updateMaxSpeedForCurva(curvaActual, me.piecePosition.lane.endLaneIndex);
                            curvaActual = curvas.Where(x => x.enEsta(me.piecePosition.pieceIndex)).FirstOrDefault();
                        }
                        else
                        {
                            if (Math.Abs(curvaActual.ultimoAnguloDerrape[me.piecePosition.lane.endLaneIndex]) < Math.Abs(me.angle))
                            {
                                curvaActual.ultimoAnguloDerrape[me.piecePosition.lane.endLaneIndex] = Math.Abs(me.angle);
                            }
                        }

                        Console.Write("\n");

                        //->
                        send(comando);
                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");
                        gameInit = JsonConvert.DeserializeObject<init.GameInit>(line);
                        

                        buscarCaminoMasCorto();
                        buscarRectaMasLarga();
                        identificarCurvas();
                        SpeedManager.setInitialSpeeds(curvas, track);

                        send(new Throttle(1, 0));
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Throttle(1, 1));
                        break;
                    case "turboAvailable":
                        turboAvailable = JsonConvert.DeserializeObject<Mensajes.In.TurboAvailable>(line);
                        Console.WriteLine("turboo! " + turboAvailable.data.turboFactor + " * " + turboAvailable.data.turboDurationTicks);
                        break;
                    case "crash":
                        //TODO Reducir velocidad en curva si fue por eso D:!

                        if (!enRecta(me.piecePosition.pieceIndex))
                        {
                            if (ultimoAngulo > 50)
                            {
                                curvaActual = curvas.Where(x => x.enEsta(me.piecePosition.pieceIndex)).FirstOrDefault();
                                SpeedManager.ajustarVelocidadPorChoque(curvaActual, me.piecePosition.lane.endLaneIndex);
                            }
                        }

                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(msg.msgType);
                        Console.ForegroundColor = ConsoleColor.Gray;
                        send(new Ping());
                        break;
                }
            }
        }

        private void identificarCurvas()
        {
            curvas = new List<Curva>();
            double ultimoAngulo = 0;
            int index = 0;

            Curva curva = new Curva(0, 0, gameInit);

            foreach (init.Piece pieza in track.pieces)
            {
                if (pieza.angle.HasValue)
                {
                    if (pieza.angle == ultimoAngulo)
                    {
                        curva.indicesPiezas.Add(index);
                    }
                    else
                    {
                        ultimoAngulo = (double)pieza.angle;
                        curva = new Curva(index, (double)pieza.angle, gameInit);
                        curvas.Add(curva);
                    }
                }
                else
                {
                    ultimoAngulo = 0;
                }

                index++;
            }
        }

        private void buscarRectaMasLarga()
        {
            int auxIndice = 0;
            double auxMayorLength = 0;
            double auxLength = 0;

            int plus = 0;

            for (int i = 0; i < track.pieces.Count; i++)
            {
                if (!track.pieces[i].angle.HasValue)
                {
                    auxLength += (double)track.pieces[i].length;
                    if (auxIndice > 0 && track.pieces[auxIndice-1].angle.HasValue)
                    {
                        auxIndice = i;
                    }

                    if (i == track.pieces.Count )
                    {
                        for (int e = 0; e < track.pieces.Count; e++)
                        {
                            if (!track.pieces[i].angle.HasValue)
                            {
                                plus++; 
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (auxLength > auxMayorLength)
                    {
                        auxMayorLength = auxLength;
                        rectaMasLargaIndex = auxIndice;
                    }

                    auxLength = 0;
                    auxIndice = i;
                }
            }

            if (auxLength > auxMayorLength)
            {
                rectaMasLargaIndex = auxIndice;
            }
        }

        private void buscarCaminoMasCorto()
        {
            int currentLane = 0;
            bestLane = new int[track.pieces.Count*gameInit.data.race.raceSession.laps+1];

            for (int currentLap = 0; currentLap < gameInit.data.race.raceSession.laps; currentLap++)
			{
                for (int i = 0; i < track.pieces.Count; i++)
			    {
                    if (track.pieces[i].@switch.HasValue && track.pieces[i].@switch == true)
                    {
                        //Por default no switcheo
                        bestLane[i + track.pieces.Count * currentLap] = currentLane;

                        //Si encuentro que swichear conviene, si
                        for (int e = i; e < track.pieces.Count; e++)
                        {
                            if (track.pieces[e].angle.HasValue)
                            {
                                for (int lindex = 0; lindex < track.lanes.Count; lindex++)
                                {
                                    if (lindex != currentLane && (lindex + 1 == currentLane || lindex - 1 == currentLane) && getPieceLenght(e, currentLane) > getPieceLenght(e, lindex))
                                    {
                                        bestLane[i + track.pieces.Count * currentLap] = lindex;
                                        currentLane = lindex;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    else
	                {
                        bestLane[i + track.pieces.Count * currentLap] = currentLane;
	                }
			    }
			}
            
        }

        private void send(SendMsg msg)
        {
            writer.WriteLine(msg.ToJson());
        }

        private double getThrottleByExpectedSpeed(double expectedSpeed)
        {
            double nextThrottle;

            if (expectedSpeed > velocidad)
            {
                nextThrottle = 1;
                while (getNextSpeedByThrottle(nextThrottle) > expectedSpeed)
                {
                    nextThrottle = nextThrottle - 0.01;
                }
            }
            else
            {
                nextThrottle = 0;
                while (getNextSpeedByThrottle(nextThrottle) < expectedSpeed)
                {
                    nextThrottle = nextThrottle + 0.01;
                }
            }

            return nextThrottle;
        }

        /// <summary>
        /// Usa la velocidad actual
        /// </summary>
        private double getNextSpeedByThrottle(double Throttle)
        {
            return velocidad + (aceleracion * Throttle - velocidad / 50);
        }

        private double getNextSpeedByThrottle(double Throttle, double vel)
        {
            return vel + (aceleracion * Throttle - vel / 50);
        }

        /// <param name="destino">Se asume lenght = 0 (el comienzo de la pieza)</param>
        /// <param name="speed">velocidad actual</param>
        /// <param name="throttle">throttle constante</param>
        /// <returns></returns>
        private int getTicksToPosition(PiecePosition origen, PiecePosition destino, double speed, double throttle)
        {
            if (origen.lap > destino.lap || (origen.lap == destino.lap && origen.pieceIndex > destino.pieceIndex))
            {
                return -1;
            }
            if (origen.lap == destino.lap && origen.pieceIndex == destino.pieceIndex)
            {
                return 0;
            }

            return getTicksToPosition_r(origen, destino, speed, throttle, 0);

        }

        private int getTicksToPosition_r(PiecePosition origen, PiecePosition destino, double speed, double throttle, int ticks)
        {
            if (ticks > 200)
            {
                return ticks;
            }

            if (origen.lap > destino.lap || (origen.lap == destino.lap && origen.pieceIndex >= destino.pieceIndex))
            {

                return ticks;
            }
            else
            {
                return getTicksToPosition_r(getNextPositionBySpeed(origen, speed), destino, getNextSpeedByThrottle(throttle, speed), throttle, ticks + 1);
            }
        }

        private PiecePosition getNextPositionBySpeed(PiecePosition origen, double speed)
        {
            double aux = origen.inPieceDistance + speed;

            if (track.pieces[origen.pieceIndex].angle.HasValue)
            {
                setPieceLenght(origen.pieceIndex, origen.lane.endLaneIndex);
            }

            if (aux > track.pieces[origen.pieceIndex].length)
            {
                PiecePosition val = new PiecePosition();
                val.inPieceDistance = aux - track.pieces[origen.pieceIndex].length;
                val.lane = origen.lane;
                val.pieceIndex = getNextPieceIndex(origen.pieceIndex);
                val.lap = origen.lap;

                if (origen.pieceIndex+1 >= track.pieces.Count)
                {
                    val.lap++;
                }

                return val;
            }
            else
            {
                origen.inPieceDistance += speed;
                return origen;
            }
        }

        private int getTicksToSpeed(double velocidadDeseada, double velocidadActual)
        {
            if (velocidadActual <= velocidadDeseada)
            {
                return 0;
            }
            return getTicksToSpeed_r(velocidadDeseada, velocidadActual, 1);
        }

        private int getTicksToSpeed_r(double velocidadDeseada, double velocidadActual, int ticks)
        {
            if (velocidadDeseada < velocidadActual)
            {
                return getTicksToSpeed_r(velocidadDeseada, velocidadActual - (velocidadActual / 50), ticks + 1);
            }
            else
            {
                return ticks;
            }
        }

        private double getNextThrottle()
        {
            double expectedSpeed = double.MaxValue;

            double maxSpeed = MAX_SPEED_X;
            int index = me.piecePosition.pieceIndex;
            while (maxSpeed == MAX_SPEED_X)
            {
                index = getNextPieceIndex(index);
                maxSpeed = piezaLineaVelocidad[index, me.piecePosition.lane.endLaneIndex];;

                //Si ya recorrí todo el array sin encontrar un maxSpeed != del maximo
                if (index == me.piecePosition.pieceIndex)
                {
                    break;
                }
            }
            int pieceIndexForMaxSpeed = index;

            expectedSpeed = getExpectedSpeedForPieceWithSpeed(maxSpeed, pieceIndexForMaxSpeed);
            if (expectedSpeed > piezaLineaVelocidad[me.piecePosition.pieceIndex, me.piecePosition.lane.endLaneIndex])
            {
                expectedSpeed = piezaLineaVelocidad[me.piecePosition.pieceIndex, me.piecePosition.lane.endLaneIndex];
            }

            Console.Write("Ex.Sp: " + expectedSpeed.ToString("0.00") + " | ");

            return getThrottleByExpectedSpeed(expectedSpeed);
        }

        private double getExpectedSpeedForPieceWithSpeed(double maxSpeed, int pieceIndexForMaxSpeed)
        {
            PiecePosition val = new PiecePosition();
            val.inPieceDistance = 0;
            val.lane = me.piecePosition.lane;
            val.pieceIndex = pieceIndexForMaxSpeed;
            val.lap = me.piecePosition.pieceIndex > pieceIndexForMaxSpeed ? me.piecePosition.lap + 1 : me.piecePosition.lap;

            if (velocidad != 0 && aceleracion != 0 && getTicksToPosition(new PiecePosition(me.piecePosition), val, velocidad, perfectThrottle) <= getTicksToSpeed(maxSpeed, velocidad))
            {
                return maxSpeed;
            }
            else
            {
                return MAX_SPEED_X;
            }
        }

        private void setPieceLenght(int pIndex, int lIndex)
        {
            int signoAngulo = (double)gameInit.data.race.track.pieces[pIndex].angle < 0 ? -1 : 1;

            gameInit.data.race.track.pieces[pIndex].length = Math.Abs((double)gameInit.data.race.track.pieces[pIndex].angle * Math.PI / 180.0
                                                                                           * ((int)gameInit.data.race.track.pieces[pIndex].radius - signoAngulo * gameInit.data.race.track.lanes[lIndex].distanceFromCenter));
        }

        private double getPieceLenght(int pIndex, int lIndex)
        {
            int signoAngulo = (double)gameInit.data.race.track.pieces[pIndex].angle < 0 ? -1 : 1;

            return Math.Abs((double)gameInit.data.race.track.pieces[pIndex].angle * Math.PI / 180.0
                                                                                           * ((int)gameInit.data.race.track.pieces[pIndex].radius - signoAngulo * gameInit.data.race.track.lanes[lIndex].distanceFromCenter));
        }

        private bool enRecta(int pieceIndex)
        {
            return (gameInit.data.race.track.pieces[pieceIndex].angle == null) 
                    || (Math.Abs((double)gameInit.data.race.track.pieces[pieceIndex].angle) == 0);
        }

        private bool nextCurva(int pieceIndex)
        {
            return !enRecta(getNextPieceIndex(pieceIndex));
        }

        private int getNextPieceIndex(int pieceIndex)
        {
            if (pieceIndex+1 >= gameInit.data.race.track.pieces.Count)
            {
                return 0;
            }
            return pieceIndex + 1;
        }

        private void updateSpeed(PiecePosition posicion)
        {
            if (posActual == null && posAnterior == null)
            {
                posActual = posicion;
                velocidad = posActual.inPieceDistance;
                return;
            }

            velocidadAnterior = velocidad;

            posAnterior = posActual;
            posActual = posicion;

            if (posAnterior.pieceIndex == posActual.pieceIndex)
            {
                velocidad = posActual.inPieceDistance - posAnterior.inPieceDistance;
            }
            else
            {
                if (gameInit.data.race.track.pieces[posAnterior.pieceIndex].angle != null)
                {
                    setPieceLenght(posAnterior.pieceIndex, posAnterior.lane.endLaneIndex);
                }

                velocidad = (gameInit.data.race.track.pieces[posAnterior.pieceIndex].length - posAnterior.inPieceDistance) + posActual.inPieceDistance;
            }
        }

        private void updateAceleracion()
        {
            if (aceleracion == 0 && velocidad != 0)
            {
                aceleracion = velocidad / perfectThrottle;
                Console.WriteLine("Aceleracion: " + aceleracion);
                Console.WriteLine("Max.Speed at " + perfectThrottle + ": " + velocidad * 50);
                Console.WriteLine("Max.Speed at 1.0: " + aceleracion * 50);

                MAX_SPEED = aceleracion * 50;
                SpeedManager.updateMaxSpeed(curvas, track);
            }
            else if(velocidad != 0 && velocidadAnterior != 0)
            {
                double fricc = -(velocidad / (velocidad - velocidadAnterior - aceleracion * perfectThrottle));
                //Console.WriteLine("Friccion: " + fricc.ToString("#0.000"));
                //Console.WriteLine((velocidad - velocidadAnterior).ToString("#0.000") + "=?" + (aceleracion * perfectThrottle - velocidad / 50).ToString("#0.000"));
            }
        }

        private void calcularFricciom()
        {
            if (calcularFriccion)
            {
                if (perfectThrottle == 0)
                {
                    calcFriccionSpeed += 1;
                }
                else
                {
                    friccion = velocidadAnterior - velocidad;
                    desaceleracion = friccion * 100 / velocidadAnterior;
                    double guess = aceleracion - velocidad / 50;
                    Console.WriteLine("V.Ant:" + velocidadAnterior.ToString("#0.00") + " V.Act:" + velocidad.ToString("#0.00") + " Menos:" + friccion.ToString("#0.00") + " %:" + desaceleracion.ToString("#0.00") + " ??:" + guess.ToString("#0.00"));
                    calcularFriccion = false;
                }
            }
        }

    }

    static class SpeedManager
    {
        public static double[,] piezaLineaVelocidad;
        private static List<Curva> Curvas;
        private static init.Track Track;

        public static void setInitialSpeeds(List<Curva> curvas, init.Track track)
        {
            Curvas = curvas;
            Track = track;

            piezaLineaVelocidad = new double[track.pieces.Count, track.lanes.Count];

            for (int i = 0; i < track.pieces.Count; i++)
            {
                for (int e = 0; e < track.lanes.Count; e++)
                {
                    if (curvas.Any(x=>x.enEsta(i)))
                    {
                        piezaLineaVelocidad[i, e] = Bot.MAX_SPEED_DEFAULT_CURVA;
                    }
                    else
                    {
                        piezaLineaVelocidad[i, e] = Bot.MAX_SPEED_X;
                    }
                    
                }
            }
        }

        public static void updateMaxSpeed(List<Curva> curvas, init.Track track)
        {
            for (int i = 0; i < track.pieces.Count; i++)
            {
                for (int e = 0; e < track.lanes.Count; e++)
                {
                    if (!curvas.Any(x => x.enEsta(i)))
                    {
                        piezaLineaVelocidad[i, e] = Bot.MAX_SPEED_X;
                    }
                }
            }
        }

        public static void replicarCambiosEnVelocidad(Curva curvaModificada, int lIndex)
        {
            foreach (Curva curva in Curvas)
            {
                for (int i = 0; i < Track.lanes.Count; i++)
                {
                    if (!curva.fixd[i])
                    {
                        if (Between(curva.lengths[i], curvaModificada.lengths[lIndex] - 1, curvaModificada.lengths[lIndex] + 1, true) && curva.angle == curvaModificada.angle)
                        {
                            for (int c = curva.primerIndice; c < curva.ultimoIndice; c++)
                            {
                                piezaLineaVelocidad[c, i] = curvaModificada.ultimaVelocidad[lIndex];
                            }
                        }
                        else if (curva.angle == curvaModificada.angle)
                        {
                            for (int c = curva.primerIndice; c < curva.ultimoIndice; c++)
                            {
                                double porcentaje = curva.lengths[i] / ( 1 + curvaModificada.lengths[lIndex] );

                                if (porcentaje > 1.5)
                                {
                                    porcentaje = 1.5;
                                }
                                else if (porcentaje < 0.8)
                                {
                                    porcentaje = 0.8;
                                }

                                piezaLineaVelocidad[c, i] = curvaModificada.ultimaVelocidad[lIndex] * porcentaje;
                            }
                        }
                    }
                }
            }
        }

        public static bool Between(double num, double lower, double upper, bool inclusive)
        {
            return inclusive ? lower <= num && num <= upper : lower < num && num < upper;
        }

        public static void updateMaxSpeedForCurva(Curva curva, int laneIndex)
        {
            if (!curva.crashed[laneIndex])
            {
                for (int i = curva.primerIndice; i < curva.ultimoIndice; i++)
                {
                    curva.anteriorAnguloDerrape = curva.ultimoAnguloDerrape;
                    curva.anteriorVelocidad = curva.ultimaVelocidad;

                    double aumento = piezaLineaVelocidad[i, laneIndex] * (60 - curva.ultimoAnguloDerrape[laneIndex]) / (1 + curva.ultimoAnguloDerrape[laneIndex] * 10d);
                    piezaLineaVelocidad[i, laneIndex] += aumento;

                    curva.ultimoAumento[laneIndex] = aumento;
                    curva.ultimaVelocidad[laneIndex] = piezaLineaVelocidad[i, laneIndex];
                }
                curva.ultimoAnguloDerrape[laneIndex] = 0;
                curva.fixd[laneIndex] = true;

                replicarCambiosEnVelocidad(curva, laneIndex);
            }
        }


        public static void ajustarVelocidadPorChoque(Curva curvaActual, int laneIndex)
        {

            if (curvaActual.crashed[laneIndex] && curvaActual.ultimaVelocidad[laneIndex] < curvaActual.velocidadCrash[laneIndex])
            {
                curvaActual.velocidadCrash[laneIndex] = curvaActual.ultimaVelocidad[laneIndex];
            }
            else
            {
                curvaActual.crashed[laneIndex] = true;
                curvaActual.velocidadCrash[laneIndex] = curvaActual.ultimaVelocidad[laneIndex];
            }


            if (curvaActual.ultimoAumento[laneIndex] > 0)
            {
                for (int i = curvaActual.primerIndice; i < curvaActual.ultimoIndice; i++)
                {
                    piezaLineaVelocidad[i, laneIndex] -= curvaActual.ultimoAumento[laneIndex];

                    if (curvaActual.anteriorAnguloDerrape[laneIndex] < 55)
                    {
                        double aumento = piezaLineaVelocidad[i, laneIndex] * (60 - curvaActual.ultimoAnguloDerrape[laneIndex]) / (1 + curvaActual.ultimoAnguloDerrape[laneIndex] * 20d);
                        piezaLineaVelocidad[i, laneIndex] += aumento;
                        curvaActual.ultimoAumento[laneIndex] = aumento;
                    }

                    curvaActual.ultimaVelocidad[laneIndex] = piezaLineaVelocidad[i, laneIndex];
                }
                curvaActual.ultimoAnguloDerrape[laneIndex] = 0;
            }
            else
            {
                //aumento en este caso es una disminución
                for (int i = curvaActual.primerIndice; i < curvaActual.ultimoIndice; i++)
                {
                    double aumento = -piezaLineaVelocidad[i, laneIndex] * 25 / 100;
                    piezaLineaVelocidad[i, laneIndex] += aumento;
                    curvaActual.ultimoAumento[laneIndex] = aumento;
                    curvaActual.ultimaVelocidad[laneIndex] = piezaLineaVelocidad[i, laneIndex];
                }
                curvaActual.ultimoAnguloDerrape[laneIndex] = 0;
            }

            replicarCambiosEnVelocidad(curvaActual, laneIndex);
        }
    }

    class Curva
    {
        private init.GameInit gameInit;
        private List<init.Lane> lanes;
        private init.Track track { get { return gameInit.data.race.track; } }

        public double angle;
        public List<int> indicesPiezas;
        public double[] lengths;
        public int primerIndice { get { return indicesPiezas.FirstOrDefault(); } }
        public int ultimoIndice { get { return indicesPiezas.LastOrDefault(); } }

        //Estadisticas de la curva
        public double[] anteriorAnguloDerrape;
        public double[] anteriorVelocidad;
        public double[] ultimoAumento;
        public double[] ultimaVelocidad;
        public double[] ultimoAnguloDerrape;
        public bool[] crashed;
        public double[] velocidadCrash;
        public bool[] fixd;

        public Curva(int startIndex, double angle, init.GameInit gameInit)
        {
            this.lanes = gameInit.data.race.track.lanes;
            this.gameInit = gameInit;
            this.angle = angle;

            indicesPiezas = new List<int>();
            indicesPiezas.Add(startIndex);
            lengths = new double[lanes.Count];

            anteriorAnguloDerrape = new double[lanes.Count];
            anteriorVelocidad = new double[lanes.Count];
            ultimoAumento = new double[lanes.Count];
            ultimaVelocidad = new double[lanes.Count];
            ultimoAnguloDerrape = new double[lanes.Count];
            crashed = new bool[lanes.Count];
            fixd = new bool[lanes.Count];
        }

        public double getLength(int lane)
        {
            if (lengths[lane] == 0)
            {
                lengths[lane] = 0;
                foreach (int pieceIndex in indicesPiezas)
                {
                    lengths[lane] += getPieceLenght(pieceIndex, lane);
                }
            }

            return lengths[lane];
        }

        public bool enEsta(int pieceIndexActual)
        {
            return indicesPiezas.Contains(pieceIndexActual);
        }

        private double getPieceLenght(int pIndex, int lIndex)
        {
            int signoAngulo = (double)gameInit.data.race.track.pieces[pIndex].angle < 0 ? -1 : 1;

            return Math.Abs((double)gameInit.data.race.track.pieces[pIndex].angle * Math.PI / 180.0
                                                                                           * ((int)gameInit.data.race.track.pieces[pIndex].radius - signoAngulo * gameInit.data.race.track.lanes[lIndex].distanceFromCenter));
        }
    }
}