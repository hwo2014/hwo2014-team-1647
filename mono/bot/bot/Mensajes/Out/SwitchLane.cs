﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace bot
{
    public class SwitchLane : SendMsg
    {
        public string data;
        public int gameTick;

        public SwitchLane(string data, int gameTick)
        {
            this.data = data;
            this.gameTick = gameTick;
        }

        protected override Object MsgData()
        {
            return this.data;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}
