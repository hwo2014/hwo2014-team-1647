﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace bot
{
    public class Turbo : SendMsg
    {
        public string data { get; set; }

        public Turbo()
        {
            data = "A por el pasto!";
        }
        protected override Object MsgData()
        {
            return this.data;
        }
        protected override string MsgType()
        {
            return "turbo";
        }
    }
}
