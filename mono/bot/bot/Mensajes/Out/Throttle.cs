﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace bot
{
    public class Throttle : SendMsg
    {
        public double value;
        public int gameTick;

        public Throttle(double value, int gameTick)
        {
            this.value = value;
            this.gameTick = gameTick;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
}
