﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bot.Mensajes.Out
{
    public class CreateRace : SendMsg
    {
        public CreateRace(string name, string key, string trackName, string password, int carCount)
        {
            botId = new BotId();
            botId.key = key;
            botId.name = name;

            this.trackName = trackName;
            this.carCount = carCount;
            this.password = password;
        }
        public BotId botId { get; set; }
        public string trackName { get; set; }
        public string password { get; set; }
        public int carCount { get; set; }

        protected override string MsgType()
        {
            return "createRace";
        }
    }

    public class BotId
    {
        public string name { get; set; }
        public string key { get; set; }
    }
}
