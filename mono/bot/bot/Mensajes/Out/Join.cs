﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace bot
{
    public class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
}
