﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace bot
{
    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}
