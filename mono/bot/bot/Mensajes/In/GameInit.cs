﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bot.init
{
    public class Piece
    {
        public double length { get; set; }
        public bool? @switch { get; set; }
        public int? radius { get; set; }
        public double? angle { get; set; }
    }

    public class Lane
    {
        public int distanceFromCenter { get; set; }
        public int index { get; set; }
    }

    public class Position
    {
        public double x { get; set; }
        public double y { get; set; }
    }

    public class StartingPoint
    {
        public Position position { get; set; }
        public double angle { get; set; }
    }

    public class Track
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<Piece> pieces { get; set; }
        public List<Lane> lanes { get; set; }
        public StartingPoint startingPoint { get; set; }
    }

    public class Id
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class Dimensions
    {
        public double length { get; set; }
        public double width { get; set; }
        public double guideFlagPosition { get; set; }
    }

    public class Car
    {
        public Id id { get; set; }
        public Dimensions dimensions { get; set; }
    }

    public class RaceSession
    {
        public int laps { get; set; }
        public int maxLapTimeMs { get; set; }
        public bool quickRace { get; set; }
    }

    public class Race
    {
        public Track track { get; set; }
        public List<Car> cars { get; set; }
        public RaceSession raceSession { get; set; }
    }

    public class Data
    {
        public Race race { get; set; }
    }

    public class GameInit
    {
        public GameInit(string msgType, Data data) 
        {
           this.msgType = msgType;
           this.data = data;
        }
        public string msgType { get; set; }
        public Data data { get; set; }
    }
}
