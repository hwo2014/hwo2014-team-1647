﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace bot
{
    public class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }
}
