﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bot
{
    public class Id
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class Lane
    {
        public int startLaneIndex { get; set; }
        public int endLaneIndex { get; set; }
    }

    public class PiecePosition
    {
        public PiecePosition() { }
        public PiecePosition(PiecePosition copy)
        {
            this.inPieceDistance = copy.inPieceDistance;
            this.lane = new Lane();
            this.lane.endLaneIndex = copy.lane.endLaneIndex;
            this.lane.startLaneIndex = copy.lane.startLaneIndex;
            this.lap = copy.lap;
            this.pieceIndex = copy.pieceIndex;
        }
        public int pieceIndex { get; set; }
        public double inPieceDistance { get; set; }
        public Lane lane { get; set; }
        public int lap { get; set; }
    }

    public class Datos
    {
        public Id id { get; set; }
        public double angle { get; set; }
        public PiecePosition piecePosition { get; set; }
    }

    public class CarPositions
    {
        public CarPositions(string msgType, Datos[] data) 
        {
           this.msgType = msgType;
           this.data = data;
        }
        public Datos[] data { get; set; }
        public string gameId { get; set; }
        public int gameTick { get; set; }
        string msgType { get; set; }
    }
}
