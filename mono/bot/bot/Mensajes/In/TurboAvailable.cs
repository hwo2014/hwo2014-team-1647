﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bot.Mensajes.In
{
    class TurboAvailable
    {
        public TurboAvailable(string msgType, Data data) 
        {
           this.msgType = msgType;
           this.data = data;
        }
        public string msgType { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public double turboDurationMilliseconds { get; set; }
        public int turboDurationTicks { get; set; }
        public double turboFactor { get; set; }
    }
}
