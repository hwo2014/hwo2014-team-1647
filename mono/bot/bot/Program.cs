﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace bot
{
    class Program
    {
        public static void Main(string[] args)
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                //keimola germany usa france
                new Bot(reader, writer, new Mensajes.Out.CreateRace(botName, botKey, "france", "", 1));
            }

            Console.WriteLine("x");
            Console.ReadKey();
        }
    }
}
